# CS418: MP2 #

### Set up: ###
No compilation is necessary, simply open the ".html" file with a WebGL
compliant browser.

See the [link](http://www.khronos.org/webgl/wiki/Getting_a_WebGL_Implementation)
for information on currently supported browsers.

### Video ###
[video link](http://youtu.be/hDkdmNJnQAQ)
