function main() {
  var canvas = document.getElementById("webgl");
  if (!canvas) {
	console.log("Failed to load canvas element");
	return;
  }

  var gl = getWebGLContext(canvas);
  if (!gl) {
	console.log("Failed to get WebGL context");
	return;
  }

  var vShaderSource = getShaderSource("shader-vs");
  var fShaderSource = getShaderSource("shader-fs");
  if (!initShaders(gl, vShaderSource, fShaderSource)) {
	console.log("Failed to initialize shaders");
	return;
  }

  // look up where the vertex data needs to go
  var positionLocation = gl.getAttribLocation(gl.program, "a_position");
  var colorLocation = gl.getAttribLocation(gl.program, "a_color");

  // look up uniforms
  gl.program.mvMatrixLocation = gl.getUniformLocation(gl.program,
	"u_mvMatrix");
  gl.program.pMatrixLocation = gl.getUniformLocation(gl.program,
	"u_pMatrix");

  // create buffer for vertices
  var buffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
  setGeometry(gl);
  gl.vertexAttribPointer(positionLocation, 3, gl.FLOAT, false, 0, 0);
  gl.enableVertexAttribArray(positionLocation);

  // create buffer for colors
  var buffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
  setColors(gl);
  gl.vertexAttribPointer(colorLocation, 4, gl.FLOAT, false, 0, 0);
  gl.enableVertexAttribArray(colorLocation)

  // create buffer for indices
  var buffer = gl.createBuffer();
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, buffer);
  setIndices(gl);

  gl.viewportWidth = canvas.width;
  gl.viewportHeight = canvas.height;

  gl.clearColor(0.0, 0.0, 0.0, 1.0);
  gl.enable(gl.DEPTH_TEST);

  var rate = 0;
  var render = function() {
	requestAnimationFrame(render);
	rate = animate();
	drawScene(gl, rate);
  }
  render();

  $(document).keypress(function(event) {
	if (event.keyCode == 97) {  // a pressed
	  camera.rotate('y', 3);
	}
	if (event.keyCode == 100) { // d pressed
	  camera.rotate('y', -3);
	}
	if (event.keyCode == 119) { // w pressed
	  camera.rotate('x', -3);
	}
	if (event.keyCode == 115) { // s pressed
	  camera.rotate('x', 3);
	}
	if (event.keyCode == 113) { // q pressed
	  camera.rotate('z', 3);
	}
	if (event.keyCode == 101) { // e pressed
	  camera.rotate('z', -3);
	}
  });
}

var then = Date.now();
function animate() {
  var EYE_STEP = 3.0; // 3.0 units per second

  var now = Date.now();
  var elapsed = now - then;
  then = now;

  return (elapsed * (1 / 1000.0)) * EYE_STEP;
}

var mvMatrix = mat4.create();
var pMatrix = mat4.create();

function setMatrixUniforms(gl) {
  gl.uniformMatrix4fv(gl.program.mvMatrixLocation, false, mvMatrix);
  gl.uniformMatrix4fv(gl.program.pMatrixLocation, false, pMatrix);
}

function Camera(position, target, up) {
  this.position = position;

  this.init(target, up || vec3.fromValues(0.0, 1.0, 0.0));
}

Camera.prototype.init = function (target, up) {
  this.zAxis = vec3.sub(vec3.create(), this.position, target);
  vec3.normalize(this.zAxis, this.zAxis);

  this.xAxis = vec3.cross(vec3.create(), up, this.zAxis);
  vec3.normalize(this.xAxis, this.xAxis);

  this.yAxis = vec3.cross(vec3.create(), this.zAxis, this.xAxis);
}

Camera.prototype.rotate = function (choice, degree) {
  var options = {
	'x': {"method": mat4.rotateX, "axis1": 2, "axis2": 3},
	'y': {"method": mat4.rotateY, "axis1": 1, "axis2": 3},
	'z': {"method": mat4.rotateZ, "axis1": 1, "axis2": 2},
  };

  var rot = options[choice];

  // create rotation matrix
  var M_rot = mat4.create();
  var viewMatrix = this.get_view_matrix();

  rot.method(M_rot, M_rot, degToRad(degree));

  // multiply rotation matrix by old points matrix
  var points = mat4.mul(
	mat4.create(),
	M_rot,
	mat4.transpose(
	  mat4.create(),
	  viewMatrix
	)
  );

  // update view matrix
  this.update_axis(extractCol(points, rot.axis1), rot.axis1);
  this.update_axis(extractCol(points, rot.axis2), rot.axis2);
}

Camera.prototype.update_axis = function(vec, axis) {
  var option = {1: 'xAxis', 2: 'yAxis', 3: 'zAxis'};

  this[option[axis]] = vec3.fromValues(vec[0], vec[1], vec[2]);
}

Camera.prototype.get_view_matrix = function() {
  var viewMatrix = mat4.create();

  insertRow(viewMatrix, 1, this.xAxis);
  insertRow(viewMatrix, 2, this.yAxis);
  insertRow(viewMatrix, 3, this.zAxis);

  return viewMatrix;
}

var camera = new Camera(
  vec3.fromValues(0.0, 0.0, 9.0),   // eye position
  vec3.fromValues(0.0, 0.0, 10.0)  // lookat position
);

function drawScene(gl, rate) {
  gl.viewport(0, 0, gl.viewportWidth, gl.viewportHeight);
  gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

  mat4.perspective(pMatrix, 45, gl.viewportWidth / gl.viewportHeight,
	0.1, 100.0);

  mat4.identity(mvMatrix);

  vec3.scaleAndAdd(camera.position, camera.position, camera.zAxis, rate);

  var viewMatrix = mat4.lookAt(
	mat4.create(),
	camera.position,
	vec3.add(vec3.create(), camera.position, camera.zAxis),
	camera.yAxis
  );

  mat4.mul(mvMatrix, viewMatrix, mvMatrix);

  // draw cubes
  mvPushMatrix();

  mat4.translate(mvMatrix, mvMatrix, [-2.5, 0.0, -7.0]);

  setMatrixUniforms(gl);
  gl.drawElements(gl.TRIANGLES, 36, gl.UNSIGNED_SHORT, 0);

  mvPopMatrix();

  mvPushMatrix();

  mat4.translate(mvMatrix, mvMatrix, [2.5, 0.0, -7.0]);

  setMatrixUniforms(gl);
  gl.drawElements(gl.TRIANGLES, 36, gl.UNSIGNED_SHORT, 0);

  mvPopMatrix();

  mvPushMatrix();

  mat4.translate(mvMatrix, mvMatrix, [-2.5, 0.0, -17.0]);

  setMatrixUniforms(gl);
  gl.drawElements(gl.TRIANGLES, 36, gl.UNSIGNED_SHORT, 0);

  mvPopMatrix();

  mvPushMatrix();

  mat4.translate(mvMatrix, mvMatrix, [2.5, 0.0, -17.0]);

  setMatrixUniforms(gl);
  gl.drawElements(gl.TRIANGLES, 36, gl.UNSIGNED_SHORT, 0);

  mvPopMatrix();
}

// Helper Functions
function degToRad(deg) {
  return deg * (Math.PI / 180);
}

function insertCol(M, col, vec) {
  col = --col * 4;
  for (var i = 0; i < vec.length; i++) {
	M[col + i] = vec[i];
  }
}

function insertRow(M, row, vec) {
  mat4.transpose(M, M);
  insertCol(M, row, vec);
  mat4.transpose(M, M);
}

function extractCol(M, col) {
  col = --col * 4;
  return [M[col], M[col+1], M[col+2], M[col+3]];
}

function extractRow(M, row) {
  var temp = mat4.clone(M);
  mat4.transpose(temp, temp);
  return extractCol(temp, row);
}

function show(M) {
  s = '  ';
  for (var i = 0; i < 4; i++) {
	console.log(M[0*4+i] + s + M[1*4+i] + s +
	M[2*4+i] + s + M[3*4+i]);
  }
}

var mvMatrixStack = [];

function mvPushMatrix() {
  var copy = mat4.create();
  mat4.copy(copy, mvMatrix);
  mvMatrixStack.push(copy);
}

function mvPopMatrix() {
  if (!mvMatrixStack.length) {
	throw("Can't pop from an empty matrix stack.");
  }

  mvMatrix = mvMatrixStack.pop();
}

function getShaderSource(id) {
  var shaderScript = $('#' + id);
  if (!shaderScript.get(0)) {
	return null;
  }

  return shaderScript.html();
}

function setGeometry(gl) {
  var cubeVertices = new Float32Array([
	// Front face
	-1.0, -1.0, 1.0,
	 1.0, -1.0, 1.0,
	 1.0,  1.0, 1.0,
	-1.0,  1.0, 1.0,

	// Back face
	-1.0, -1.0, -1.0,
	-1.0,  1.0, -1.0,
	 1.0,  1.0, -1.0,
	 1.0, -1.0, -1.0,

	// Top face
	-1.0, 1.0, -1.0,
	-1.0, 1.0,  1.0,
	 1.0, 1.0,  1.0,
	 1.0, 1.0, -1.0,

	// Bottom face
	-1.0, -1.0, -1.0,
	 1.0, -1.0, -1.0,
	 1.0, -1.0,  1.0,
	-1.0, -1.0,  1.0,

	// Right face
	1.0, -1.0, -1.0,
	1.0,  1.0, -1.0,
	1.0,  1.0,  1.0,
	1.0, -1.0,  1.0,

	// Left face
	-1.0, -1.0, -1.0,
	-1.0, -1.0,  1.0,
	-1.0,  1.0,  1.0,
	-1.0,  1.0, -1.0
  ]);
  gl.bufferData(gl.ARRAY_BUFFER, cubeVertices, gl.STATIC_DRAW);
}

function setColors(gl) {
  colors = [
	[1.0, 0.0, 0.0, 1.0], // Front face
	[1.0, 1.0, 0.0, 1.0], // Back face
	[0.0, 1.0, 0.0, 1.0], // Top face
	[1.0, 0.5, 0.5, 1.0], // Bottom face
	[1.0, 0.0, 1.0, 1.0], // Right face
	[0.0, 0.0, 1.0, 1.0]  // Left face
  ];

  var unpackedColors = [];
  for (var i in colors) {
	var color = colors[i];
	for (var j = 0; j < 4; j++) {
	  unpackedColors = unpackedColors.concat(color);
	}
  }

  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(unpackedColors),
	gl.STATIC_DRAW);
}

function setIndices(gl) {
  var cubeVertexIndices = new Uint16Array([
	0,  1,  2,    0,  2,  3,   // Front face
	4,  5,  6,    4,  6,  7,   // Back face
	8,  9,  10,   8,  10, 11,  // Top face
	12, 13, 14,   12, 14, 15,  // Bottom face
	16, 17, 18,   16, 18, 19,  // Right face
	20, 21, 22,   20, 22, 23   // Left face
  ]);
  gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, cubeVertexIndices, gl.STATIC_DRAW);
}
